import {Injectable} from '@angular/core';

import {Recipe} from './recipe.model';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {

  private recipes: Recipe[] = [
    new Recipe(
      'Mediterranian Pasta',
      'This Mediterranean Pasta is the breezy social butterfly of the dinner recipe crowd.',
      '../assets/Mediterranean-Pasta-recipe-600x792.jpg',
      [
        new Ingredient( 'Pasta', 3),
        new Ingredient( 'Sea products', 2),
      ]),
    new Recipe(
      'Creole Shrimp',
      'A spoonful of Cajun seasoning turns this shrimp skillet from ordinary to extraordinary!' +
      ' With only 10 minutes of prep, you won\'t believe how easy it is to serve up our Quick Creole Shrimp recipe.',
      'https://assets.kraftfoods.com/recipe_images/opendeploy/115893_MXM_K57644V0EC_OR1_CR_640x428.jpg',
      [
        new Ingredient('Mushrooms', 4),
        new Ingredient('Tomatoes', 1)
      ]
    ),
    new Recipe('Chili',
      'Simple, Perfect Chili Recipe',
      'https://food.fnr.sndimg.com/content/dam/images/food/fullset/2012/2/1/0/' +
      'WU0202_chili_s3x4.jpg.rend.hgtvcom.826.620.suffix/1486076474733.jpeg',
      [
        new Ingredient('Beans', 10),
        new Ingredient( 'Chili', 2)
      ])
  ];

  constructor(private shoppingListService: ShoppingListService) {
  }

  public getRecipes(): Recipe[] {
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]): void {
    this.shoppingListService.addIngredients(ingredients);
  }
  public getRecipeById(index: number): Recipe {
    return this.recipes[index];
  }
}
