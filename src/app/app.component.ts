import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'RecipesBook';
  page2nav = 'recipe';

  onPageSelected(page: string): void {
    this.page2nav = page;
  }
}
